package main

import (
	"bytes"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"

	"gopkg.in/ini.v1"
)

const version = "0.2"
const name = "midpoint-task-dashboard"
const faviconPngBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAHrElEQVRYR52Xe1BU1x3HP+fey64sC7uLICIq4AMktaAhjpLYSn2kNlGLjdbm0cRMbH2MY21tNKaNj5nEaKc1xKad6TS2PqqmjaKTUVujqTiOiUqpoBGf5WUBxRdPl4W993TO8nChgLS/f/fuOZ/z+/2+v4egjyaldJuYk7DIOnk4J+Hcsb0JNpstIXPuD0hKzyj1+5pLbfZ+pWgc0NFPCCFq+nK0eNRHUsoEC2utr6lpvmlanDn+V/Z9sAbv/UoEgrjho4kcksLNO/eZ9cJrZEyZjpQWSLbpmr5eCFHa2x09AqgXq4vvVd9anntgF8UX82hqaOBWZSm1tyswNIGmKQSJZQlMv5/RX3uWpRs/xB4aClIG7tWFvg54vyePdAugLgeO+5p9Y36z/qfk5vyR8FCBPURDCA2EhmlKpCVoajERuo7HoTEgbjiLN/+ZmLihSFoBFKJAFADf6A7ivwCklGPU5YC7vKyElS/PoPFWCTabgaOfgaELUC5GJ8Qw8JsW9Q+aaTElHreH5Zt3kpSaTkN9Ay1+PwOiozGMEMWickJBKJgO6wTQ9vISdbn6ouJGGavmz6Su4jpC0wIv1YRgXNJgYmOiqKy+S2pyIqH9bFwtrSS/qJio+FGEu9zU1dXg97fwwrL1pE+c2n6hgkgM9kQHQLvbAeWBgDW3NLPp9YWcPrwHwzCwpMTdz85zE1PB0CivqsZCJ+ubTxEb4+LoyQL2//0MljTRdR2/ZmP+z35L5jPfCX50p3AEA2QDP+qasYX5p9ny86XcLbtMSmIcVksTU9NGIsMi8EuDloYaYmKjKK+6Q9XdWi5cLUFKP75mk6gRT/Dm+zsZMHBQ12OVOlRyEgBQUgOU6zuZJS2EEJzPP8PfdmxhQqSX8qrbjEtJ5HKNoNExiPHDXVy/fIHqWzUUFF2htPI2Gham3ckP38rmW7O/15MKVShK2wG2Aa90B6CyWRM6NberKNi1kRuX/4kj1MaIhFgGxMZiABcraqhrlBw8eIiyOzVEREaTtWglc15cgN1uD+igG9suhJgv2mJ/v7svlAc65CQ0dmxazcn9HzJ4SDyhA5MI81Zg83txhYdimTpfVFkkpI7j608/y5jxE9ENvV2GPXnBowCygP2PBhCUXb/Cng/epaWskPQ5ywiPSyRcNlBy7TLF504xe9nbJCY/hqbpgURUpgmtJw+on2crgG7dr35t94A6REoVCo3y61f4/VsLGBHrIX3qt4lK/Aq1tQ2cOrib6QvXEDskPvC/dnsEwHYFkAtM6skD6rCL5/KIjo1jQEwsTV4vR9a/yfm8Q4QOcOKJiECzheAYOpZZS9YSFhERgO0jwAkFoJpFfLcAWPj9JtlvLKR/7BAyps3ifnUV3oIL1B76hLr6mzQPdPCgUTL5J++QOnN2h+v7CFCgAB7idqFQCZh34gjbf7EaX5OXkWOeZExGJq4IF95LV2nZ+TGOygqanC6iNqxj9Jw5iC7HPSIE6vueAZQG/vTuCo7nbEUz7Pjox5R5ixgxYiS2fnbqrxXj2/YRpseN/urzpKSmEZ88GsRD2fUFoMcQNDbU896KlykpOI4wQvBJGxNmvMrwpFF4IpxYQtB0owJ/fSNGf4Oh9kbcE+biGZLUOhM8WgWFvSbhlYLTbFr2IrLpHkaIAc4Ylm7YGpgB7lWU4Qp34jdNmv1+7v6rgMbis0x46Q3ix2Yi+ybDQBL2KMPr+Sf5+He/pLT4MpqUDE+fxGurN+LzNXMm9xix0W40JL7mFs7/4xR5n+7l9fd2MuqJSX2tAwEZ9liIVDttbKijuqoCyzQZNDSRwtxPuHblEtcuVaFFDcEzLJnH4vtz4vhnuJurWLJ+C06Xp0OKfSlEqvd3X4ppjWNrLReYlp+jOzZz8fOj1N8VXKoJwZPxNJMzHufM5yeYM+VxMiY/00mKvQEIZW3dsNdmFKzOK1evcv6LXCqr69leEk61DMMZHsa4uDC2LpqEzW7rJOZeAFqb0aPacXszaj1VcOHLLzl1IIei4kp2G0/RaOr4dRtrpo9k3XOpmF2qSi8AD9txG4QaENYG4wd3w1ZJ6Xx28DCH1v2YMvcwSpNnUtYoqLNFsmVeKounJGP2rRB1HkjaAFQuqL6Q1g7RFUDtBSuz9yGK8yjSBxMaGc3p0lru6B42P5/GsmkpfQEoBDLb58LuhlJVmFzB3bD19YKikirmvrMPp8vF7WaD0S4/ef9uQnhimDXKxa++Px6nw9HRRbspRLVAQrdDafur28Zy5QlXsAd0obHnWD6r/nIOyx6B3XefpCgHFdJBTEgjCQ+usWTedEJkC+6YOAbFjwxUw6AcUJerl/c8lgdBBMJhSSvt4YIBG/ae5UheES7Dj1eG4rx5jpaKfFLcfqLtfmwOD0ZEFDMWrCIh+avBAJ3cHpxnva9mlrVcChlITKXYXR/lcDbn1wwM8aJFjSJpwjRs1gOENPE21BLqjCDtySnEDUvu6AVCiPUaWvb/tJoFE6qJ2bTMdZqmvXJoezbHdmdjM2yEhkeStfRtxk6cFpgb1c4QKFhqcWpdTrfrmr7u/15Ouw4oanjNP/lp5v4/bMkqL7+RIGx290uLV6RNnfVdVXYLJbIGSft6ntvX9fw/HeuMOgu5DCQAAAAASUVORK5CYII="

var faviconPng = string(first(base64.StdEncoding.DecodeString(faviconPngBase64)))

var tasksOid []string
var port string
var url, user, password string
var responseBody string
var logFile io.Writer

type Task struct {
	Name                        string
	Result                      string
	Progress                    string
	LastRunStart, LastRunFinish string
}

type XmlTask struct {
	XMLName xml.Name `xml:"object"`
	Text    string   `xml:",chardata"`
	T       string   `xml:"t,attr"`
	Xmlns   string   `xml:"xmlns,attr"`
	Apti    string   `xml:"apti,attr"`
	C       string   `xml:"c,attr"`
	Icfs    string   `xml:"icfs,attr"`
	Org     string   `xml:"org,attr"`
	Q       string   `xml:"q,attr"`
	Ri      string   `xml:"ri,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Type    string   `xml:"type,attr"`
	Object  []struct {
		Text        string `xml:",chardata"`
		Oid         string `xml:"oid,attr"`
		Version     string `xml:"version,attr"`
		Xsi         string `xml:"xsi,attr"`
		Type        string `xml:"type,attr"`
		Name        string `xml:"name"`
		Description string `xml:"description"`
		Extension   struct {
			Text                             string `xml:",chardata"`
			Mext                             string `xml:"mext,attr"`
			LastReconciliationStartTimestamp string `xml:"lastReconciliationStartTimestamp"`
			WorkerThreads                    string `xml:"workerThreads"`
			Objectclass                      string `xml:"objectclass"`
			Kind                             string `xml:"kind"`
		} `xml:"extension"`
		Metadata struct {
			Text             string `xml:",chardata"`
			RequestTimestamp string `xml:"requestTimestamp"`
			RequestorRef     struct {
				Text     string `xml:",chardata"`
				Oid      string `xml:"oid,attr"`
				Relation string `xml:"relation,attr"`
				Type     string `xml:"type,attr"`
			} `xml:"requestorRef"`
			CreateTimestamp string `xml:"createTimestamp"`
			CreatorRef      struct {
				Text     string `xml:",chardata"`
				Oid      string `xml:"oid,attr"`
				Relation string `xml:"relation,attr"`
				Type     string `xml:"type,attr"`
			} `xml:"creatorRef"`
			CreateChannel string `xml:"createChannel"`
		} `xml:"metadata"`
		OperationExecution struct {
			Text       string `xml:",chardata"`
			ID         string `xml:"id,attr"`
			RecordType string `xml:"recordType"`
			Timestamp  string `xml:"timestamp"`
			Operation  struct {
				Text        string `xml:",chardata"`
				ObjectDelta struct {
					Text       string `xml:",chardata"`
					ChangeType string `xml:"changeType"`
					ObjectType string `xml:"objectType"`
				} `xml:"objectDelta"`
				ExecutionResult struct {
					Text       string `xml:",chardata"`
					Operation  string `xml:"operation"`
					Status     string `xml:"status"`
					Importance string `xml:"importance"`
					Token      string `xml:"token"`
				} `xml:"executionResult"`
				ObjectName string `xml:"objectName"`
			} `xml:"operation"`
			Status       string `xml:"status"`
			InitiatorRef struct {
				Text     string `xml:",chardata"`
				Oid      string `xml:"oid,attr"`
				Relation string `xml:"relation,attr"`
				Type     string `xml:"type,attr"`
			} `xml:"initiatorRef"`
			Channel string `xml:"channel"`
		} `xml:"operationExecution"`
		Assignment struct {
			Text     string `xml:",chardata"`
			ID       string `xml:"id,attr"`
			Metadata struct {
				Text             string `xml:",chardata"`
				RequestTimestamp string `xml:"requestTimestamp"`
				RequestorRef     struct {
					Text     string `xml:",chardata"`
					Oid      string `xml:"oid,attr"`
					Relation string `xml:"relation,attr"`
					Type     string `xml:"type,attr"`
				} `xml:"requestorRef"`
				CreateTimestamp string `xml:"createTimestamp"`
				CreatorRef      struct {
					Text     string `xml:",chardata"`
					Oid      string `xml:"oid,attr"`
					Relation string `xml:"relation,attr"`
					Type     string `xml:"type,attr"`
				} `xml:"creatorRef"`
				CreateChannel string `xml:"createChannel"`
			} `xml:"metadata"`
			TargetRef struct {
				Text     string `xml:",chardata"`
				Oid      string `xml:"oid,attr"`
				Relation string `xml:"relation,attr"`
				Type     string `xml:"type,attr"`
			} `xml:"targetRef"`
			Activation struct {
				Text            string `xml:",chardata"`
				EffectiveStatus string `xml:"effectiveStatus"`
			} `xml:"activation"`
		} `xml:"assignment"`
		Iteration      string `xml:"iteration"`
		IterationToken string `xml:"iterationToken"`
		ArchetypeRef   struct {
			Text     string `xml:",chardata"`
			Oid      string `xml:"oid,attr"`
			Relation string `xml:"relation,attr"`
			Type     string `xml:"type,attr"`
		} `xml:"archetypeRef"`
		RoleMembershipRef struct {
			Text     string `xml:",chardata"`
			Oid      string `xml:"oid,attr"`
			Relation string `xml:"relation,attr"`
			Type     string `xml:"type,attr"`
		} `xml:"roleMembershipRef"`
		TaskIdentifier string `xml:"taskIdentifier"`
		OwnerRef       struct {
			Text     string `xml:",chardata"`
			Oid      string `xml:"oid,attr"`
			Relation string `xml:"relation,attr"`
			Type     string `xml:"type,attr"`
		} `xml:"ownerRef"`
		Channel         string `xml:"channel"`
		ExecutionStatus string `xml:"executionStatus"`
		SchedulingState string `xml:"schedulingState"`
		Category        string `xml:"category"`
		HandlerUri      string `xml:"handlerUri"`
		ResultStatus    string `xml:"resultStatus"`
		ObjectRef       struct {
			Text     string `xml:",chardata"`
			Oid      string `xml:"oid,attr"`
			Relation string `xml:"relation,attr"`
			Type     string `xml:"type,attr"`
		} `xml:"objectRef"`
		LastRunStartTimestamp  string `xml:"lastRunStartTimestamp"`
		LastRunFinishTimestamp string `xml:"lastRunFinishTimestamp"`
		Progress               string `xml:"progress"`
		StructuredProgress     struct {
			Text              string `xml:",chardata"`
			CurrentPartUri    string `xml:"currentPartUri"`
			CurrentPartNumber string `xml:"currentPartNumber"`
			ExpectedParts     string `xml:"expectedParts"`
			Part              []struct {
				Text     string `xml:",chardata"`
				ID       string `xml:"id,attr"`
				PartUri  string `xml:"partUri"`
				Complete string `xml:"complete"`
				Closed   struct {
					Text    string `xml:",chardata"`
					ID      string `xml:"id,attr"`
					Outcome struct {
						Text    string `xml:",chardata"`
						Outcome string `xml:"outcome"`
					} `xml:"outcome"`
					Count string `xml:"count"`
				} `xml:"closed"`
			} `xml:"part"`
		} `xml:"structuredProgress"`
		OperationStats struct {
			Text                                string `xml:",chardata"`
			EnvironmentalPerformanceInformation struct {
				Text                   string `xml:",chardata"`
				ProvisioningStatistics struct {
					Text  string `xml:",chardata"`
					Entry []struct {
						Text        string `xml:",chardata"`
						ResourceRef struct {
							Text       string `xml:",chardata"`
							Oid        string `xml:"oid,attr"`
							Type       string `xml:"type,attr"`
							TargetName string `xml:"targetName"`
						} `xml:"resourceRef"`
						ObjectClass string `xml:"objectClass"`
						Operation   []struct {
							Text      string `xml:",chardata"`
							Operation string `xml:"operation"`
							Status    string `xml:"status"`
							Count     string `xml:"count"`
							TotalTime string `xml:"totalTime"`
							MinTime   string `xml:"minTime"`
							MaxTime   string `xml:"maxTime"`
						} `xml:"operation"`
					} `xml:"entry"`
				} `xml:"provisioningStatistics"`
				MappingsStatistics struct {
					Text  string `xml:",chardata"`
					Entry []struct {
						Text        string `xml:",chardata"`
						Object      string `xml:"object"`
						Count       string `xml:"count"`
						AverageTime string `xml:"averageTime"`
						MinTime     string `xml:"minTime"`
						MaxTime     string `xml:"maxTime"`
						TotalTime   string `xml:"totalTime"`
					} `xml:"entry"`
				} `xml:"mappingsStatistics"`
				NotificationsStatistics string `xml:"notificationsStatistics"`
				LastMessageTimestamp    string `xml:"lastMessageTimestamp"`
				LastMessage             string `xml:"lastMessage"`
			} `xml:"environmentalPerformanceInformation"`
			IterativeTaskInformation struct {
				Text string `xml:",chardata"`
				Part []struct {
					Text      string `xml:",chardata"`
					ID        string `xml:"id,attr"`
					PartUri   string `xml:"partUri"`
					Execution struct {
						Text           string `xml:",chardata"`
						ID             string `xml:"id,attr"`
						StartTimestamp string `xml:"startTimestamp"`
						EndTimestamp   string `xml:"endTimestamp"`
					} `xml:"execution"`
					Processed struct {
						Text    string `xml:",chardata"`
						ID      string `xml:"id,attr"`
						Outcome struct {
							Text    string `xml:",chardata"`
							Outcome string `xml:"outcome"`
						} `xml:"outcome"`
						Count    string `xml:"count"`
						Duration string `xml:"duration"`
						LastItem struct {
							Text           string `xml:",chardata"`
							Name           string `xml:"name"`
							DisplayName    string `xml:"displayName"`
							Type           string `xml:"type"`
							Oid            string `xml:"oid"`
							StartTimestamp string `xml:"startTimestamp"`
							EndTimestamp   string `xml:"endTimestamp"`
							OperationId    string `xml:"operationId"`
						} `xml:"lastItem"`
					} `xml:"processed"`
				} `xml:"part"`
			} `xml:"iterativeTaskInformation"`
			SynchronizationInformation struct {
				Text       string `xml:",chardata"`
				Transition []struct {
					Text                   string `xml:",chardata"`
					ID                     string `xml:"id,attr"`
					OnProcessingStart      string `xml:"onProcessingStart"`
					OnSynchronizationStart string `xml:"onSynchronizationStart"`
					OnSynchronizationEnd   string `xml:"onSynchronizationEnd"`
					Counter                struct {
						Text    string `xml:",chardata"`
						ID      string `xml:"id,attr"`
						Outcome struct {
							Text    string `xml:",chardata"`
							Outcome string `xml:"outcome"`
						} `xml:"outcome"`
						Count string `xml:"count"`
					} `xml:"counter"`
				} `xml:"transition"`
			} `xml:"synchronizationInformation"`
			ActionsExecutedInformation struct {
				Text               string `xml:",chardata"`
				ObjectActionsEntry []struct {
					Text                         string `xml:",chardata"`
					ObjectType                   string `xml:"objectType"`
					Operation                    string `xml:"operation"`
					Channel                      string `xml:"channel"`
					TotalSuccessCount            string `xml:"totalSuccessCount"`
					LastSuccessObjectName        string `xml:"lastSuccessObjectName"`
					LastSuccessObjectDisplayName string `xml:"lastSuccessObjectDisplayName"`
					LastSuccessObjectOid         string `xml:"lastSuccessObjectOid"`
					LastSuccessTimestamp         string `xml:"lastSuccessTimestamp"`
					TotalFailureCount            string `xml:"totalFailureCount"`
				} `xml:"objectActionsEntry"`
				ResultingObjectActionsEntry []struct {
					Text                         string `xml:",chardata"`
					ObjectType                   string `xml:"objectType"`
					Operation                    string `xml:"operation"`
					Channel                      string `xml:"channel"`
					TotalSuccessCount            string `xml:"totalSuccessCount"`
					LastSuccessObjectName        string `xml:"lastSuccessObjectName"`
					LastSuccessObjectDisplayName string `xml:"lastSuccessObjectDisplayName"`
					LastSuccessObjectOid         string `xml:"lastSuccessObjectOid"`
					LastSuccessTimestamp         string `xml:"lastSuccessTimestamp"`
					TotalFailureCount            string `xml:"totalFailureCount"`
				} `xml:"resultingObjectActionsEntry"`
			} `xml:"actionsExecutedInformation"`
			RepositoryPerformanceInformation struct {
				Text      string `xml:",chardata"`
				Operation []struct {
					Text            string `xml:",chardata"`
					Name            string `xml:"name"`
					InvocationCount string `xml:"invocationCount"`
					ExecutionCount  string `xml:"executionCount"`
					TotalTime       string `xml:"totalTime"`
					MinTime         string `xml:"minTime"`
					MaxTime         string `xml:"maxTime"`
					TotalWastedTime string `xml:"totalWastedTime"`
					MinWastedTime   string `xml:"minWastedTime"`
					MaxWastedTime   string `xml:"maxWastedTime"`
				} `xml:"operation"`
			} `xml:"repositoryPerformanceInformation"`
			CachesPerformanceInformation struct {
				Text  string `xml:",chardata"`
				Cache []struct {
					Text              string `xml:",chardata"`
					Name              string `xml:"name"`
					HitCount          string `xml:"hitCount"`
					WeakHitCount      string `xml:"weakHitCount"`
					MissCount         string `xml:"missCount"`
					PassCount         string `xml:"passCount"`
					NotAvailableCount string `xml:"notAvailableCount"`
				} `xml:"cache"`
			} `xml:"cachesPerformanceInformation"`
			OperationsPerformanceInformation struct {
				Text      string `xml:",chardata"`
				Operation []struct {
					Text            string `xml:",chardata"`
					Name            string `xml:"name"`
					InvocationCount string `xml:"invocationCount"`
					TotalTime       string `xml:"totalTime"`
					MinTime         string `xml:"minTime"`
					MaxTime         string `xml:"maxTime"`
				} `xml:"operation"`
			} `xml:"operationsPerformanceInformation"`
			WorkBucketManagementPerformanceInformation struct {
				Text      string `xml:",chardata"`
				Operation []struct {
					Text      string `xml:",chardata"`
					Name      string `xml:"name"`
					Count     string `xml:"count"`
					TotalTime string `xml:"totalTime"`
					MinTime   string `xml:"minTime"`
					MaxTime   string `xml:"maxTime"`
				} `xml:"operation"`
			} `xml:"workBucketManagementPerformanceInformation"`
			CachingConfiguration string `xml:"cachingConfiguration"`
			Timestamp            string `xml:"timestamp"`
		} `xml:"operationStats"`
		WorkState struct {
			Text   string `xml:",chardata"`
			Bucket struct {
				Text             string `xml:",chardata"`
				ID               string `xml:"id,attr"`
				SequentialNumber string `xml:"sequentialNumber"`
				State            string `xml:"state"`
			} `xml:"bucket"`
			NumberOfBuckets string `xml:"numberOfBuckets"`
			AllWorkComplete string `xml:"allWorkComplete"`
		} `xml:"workState"`
		ExpectedTotal    string `xml:"expectedTotal"`
		Recurrence       string `xml:"recurrence"`
		Binding          string `xml:"binding"`
		ThreadStopAction string `xml:"threadStopAction"`
		Result           struct {
			Text       string `xml:",chardata"`
			Incomplete string `xml:"incomplete,attr"`
		} `xml:"result"`
	} `xml:"object"`
}

func first(n []byte, _ error) []byte {
	return n
}

func faviconFile(w http.ResponseWriter, request *http.Request) {
	w.Header().Set("Content-type", "image/png")
	fmt.Fprint(w, faviconPng)
}

func indexPage(w http.ResponseWriter, request *http.Request) {
	//if request.Method == "OPTIONS" {
	//	w.WriteHeader(http.StatusOK)
	//	return
	//}

	loadData()

	//Allow CORS here By * or specific origin
	//w.Header().Set("Access-Control-Allow-Origin", "*")
	//w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	//w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	fmt.Fprintf(w, responseBody)
}

func refreshPage(w http.ResponseWriter, request *http.Request) {
	loadData()
	fmt.Fprintf(w, "REFRESH DONE :-)")
}

func reloadPage(w http.ResponseWriter, request *http.Request) {
	loadIni()
	loadData()
	fmt.Fprintf(w, "RELOAD and REFRESH DONE :-)")
}

func dataPage(w http.ResponseWriter, request *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, `<html>AAAAAAAABBB<html>`)
	//<tr><td> Helios A users </td><td> success </td><td> 3870 </td><td> 2021-10-12T18:02:42 </td><td> 2021-10-12T18:24:35 </td></tr>
	//<tr><td> Helios B users </td><td> success </td><td> 31 </td><td> 2021-10-12T12:38:33 </td><td> 2021-10-12T12:38:48 </td></tr>
}

func loadIni() {
	cfg, err := ini.ShadowLoad(name + ".ini")
	if err != nil {
		console("Fail to read file: %v", err)
		os.Exit(1)
	}

	url = cfg.Section("").Key("api.url").String()
	user = cfg.Section("").Key("api.user").String()
	password = cfg.Section("").Key("api.password").String()

	tasksOid = cfg.Section("").Key("task.oid").ValueWithShadows()
	//fmt.Println("Debug ", tasksOid)

	port = cfg.Section("").Key("port").String()
	log.Print("Load configuration")
}

func loadDataFromMidpoint(tasks *[]Task) {

	client := &http.Client{}

	var reqBody = []byte(`
<q:query xmlns:q="http://prism.evolveum.com/xml/ns/public/query-3">
    <filter>
        <inOid>
            <value>` + strings.Join(tasksOid[:], `</value>
			<value>`) + `</value>
        </inOid>
    </filter>
</q:query>`)

	req, err := http.NewRequest("POST", url+"tasks/search", bytes.NewBuffer(reqBody))
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	req.Header.Set("Accept", "application/xml")
	req.Header.Set("Content-Type", "application/xml")
	req.SetBasicAuth(user, password)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Print(err.Error())
		return
	}
	//fmt.Println("DATA: " + string(responseBody))

	var data XmlTask
	xml.Unmarshal(responseBody, &data)
	//fmt.Printf("OBJECTS: %v\n", data)

	xmlTasks := data.Object
	for _, i := range xmlTasks {
		newTask := Task{i.Name, i.ResultStatus, i.Progress,
			strings.Split(i.LastRunStartTimestamp, ".")[0],
			strings.Split(i.LastRunFinishTimestamp, ".")[0]}
		//fmt.Printf("ADD: %v\n", i.Name)
		*tasks = append(*tasks, newTask)
	}
	log.Print("Refresh data from Midpoint")
}

func makeTemplate(myTasks []Task) string {
	buf, err := ioutil.ReadFile("index2.template")
	if err != nil {
		fmt.Print(err)
	}
	return string(buf)
}

func makeTemplate2(myTasks []Task) string {
	buf, err := ioutil.ReadFile("index.template")
	if err != nil {
		fmt.Print(err)
	}
	master := string(buf)

	masterTmpl, err := template.New("master").Parse(master)
	if err != nil {
		log.Fatal(err)
	}
	//myTasks := []Task{Task{"recompute", "ready", "10.10.2021", "v", "v"}, Task{"reconcile", "suspended", "25.1.2022", "v", "v"}}

	var buffer bytes.Buffer
	if err := masterTmpl.Execute(&buffer, myTasks); err != nil {
		log.Fatal(err)
	}
	log.Print("Refresh template")
	return buffer.String()
}

func establishLogFile() {
	logFile, err := os.OpenFile(name+".log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	//defer logFile.Close()
	log.SetOutput(logFile)
}

func loadData() {
	tasks := []Task{}
	//loadDataFromMidpoint(&tasks)

	responseBody = makeTemplate(tasks)
}

func console(format string, n ...interface{}) {
	log.Printf(format, n...)
	fmt.Printf(format+"\n", n...)
}

func main() {
	establishLogFile()
	console("Midpoint task dashboard %s", version)

	loadIni()

	loadData()

	// make http server
	console("Start serving on port %s", port)
	http.HandleFunc("/", indexPage)
	http.HandleFunc("/refresh", refreshPage)
	http.HandleFunc("/reload", reloadPage)
	http.HandleFunc("/data", dataPage)
	http.HandleFunc("/favicon.png", faviconFile)
	http.ListenAndServe(":"+port, nil)

	console("Application ended")
}
